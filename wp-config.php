<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bmdi-training-acf' );

/** MySQL database username */
define( 'DB_USER', 'bmdi-training' );

/** MySQL database password */
define( 'DB_PASSWORD', '3wptraining2' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c*cO|Yu_S]8QV{<y]4/8@|9-{ohfzR]k0r6C~D@?&ok|,)| O {U>_M/85YcHC<d');
define('SECURE_AUTH_KEY',  'W5P/0so>x7/Dw|j~-J.u)hg3 G|!!0z8LMw_5V*E8irXz5Otx2Jt}24JGg6!!67t');
define('LOGGED_IN_KEY',    '_}8gg#8DI}yK:8]z7&T-DVA@}3s70TU|L,d4unso|].#Rl+g>/|_$9rdZ2&.w-|[');
define('NONCE_KEY',        '~!jF$|n[qc-f.gUIIkyME@id%q+}j-(>,~Q(vEWy{!$Lv` T9?9SZXy0@B(/@*V|');
define('AUTH_SALT',        '.Tf|XXTbo0I&bxmJ|p-7|OIysZ}zu_A)pZU:;0`8n$H&^i*JnTo>_w.a&0*_4C|*');
define('SECURE_AUTH_SALT', '5~N9W!qdUukB&qB@F-gKv2$yX|wz+|*&brYb,{c=08g%g5UDj]ym?sN+jM~NbXq;');
define('LOGGED_IN_SALT',   'LFs%Qo1/lR1`~~{2!t);)~fTL[}!,63BK4sd!VBIIsiz_D)5of~y?Be[Mk#+H${B');
define('NONCE_SALT',       'gCV3ES--:?hh*Zv^5yq_@VP,/Pb~3Nn]C )|t|L(+rEr)M-C^U,M[|f^P+g_-(1/');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

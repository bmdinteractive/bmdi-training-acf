<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( is_sticky() && is_home() ) :
			echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
		endif;
	?>
	<header class="entry-header">
		<?php
			if ( 'post' === get_post_type() ) :
				echo '<div class="entry-meta">';
					if ( is_single() ) :
						twentyseventeen_posted_on();
					else :
						echo twentyseventeen_time_link();
						twentyseventeen_edit_link();
					endif;
				echo '</div><!-- .entry-meta -->';
			endif;

			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		?>
		<?php if(has_category('books')): ?>
			<div class="author-name">by <?php the_field('author_name'); ?></div>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

<div class="entry-content">
	<?php if(has_category('books')): ?>
	<div class="publication-date"><strong>Published:</strong> <?php the_field('publication_year'); ?></div>
	<?php endif; ?>
	<?php
		/* translators: %s: Name of current post */

		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );
	?>
	<?php
	if(has_category('music') && is_single()):
		
		if(get_field('audio_embed')):
		?>
		<h3>Listen:</h3>
		<?php
			the_field('audio_embed');
		endif;

		if(get_field('video_embed')):
		?>
		<h3>Watch:</h3>
		<?php
			the_field('video_embed');
		endif;

	endif;
	?>
<?php
if(has_category('books') && is_single()):
	if(get_field('favorite_quotes')):
		?>
		<h3>Favorite Quotations:</h3>
		<blockquote>
		<?php
		the_field('favorite_quotes');
		?>
		</blockquote>
		<?php
	endif;
endif;
?>
	<?php
	if(has_category(['books','music']) && is_single()):
		if(get_field('artist_website') || get_field('artist_soundcloud') || get_field('artist_spotify') || get_field('artist_twitter') || get_field('artist_instagram') || get_field('artist_youtube')):
			?>
		<h3>Follow this artist:</h3>
		<div class="artist-links social-navigation">
			<ul>
			<?php if(get_field('artist_website')): ?>
				<li>
					<a href="<?php the_field('artist_website'); ?>" target="_blank" title="Official Website">
					<?php echo twentyseventeen_get_svg(array('icon'=>'chain')); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if(get_field('artist_soundcloud')): ?>
				<li><a href="<?php the_field('artist_soundcloud'); ?>" target="_blank" title="Soundcloud">
					<?php echo twentyseventeen_get_svg(array('icon'=>'soundcloud')); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if(get_field('artist_spotify')): ?>
				<li><a href="<?php the_field('artist_spotify'); ?>" target="_blank" title="Spotify">
					<?php echo twentyseventeen_get_svg(array('icon'=>'spotify')); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if(get_field('artist_youtube')): ?>
				<li><a href="<?php the_field('artist_youtube'); ?>" target="_blank" title="Youtube">
					<?php echo twentyseventeen_get_svg(array('icon'=>'youtube')); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if(get_field('artist_instagram')): ?>
				<li><a href="<?php the_field('artist_instagram'); ?>" target="_blank" title="Instagram">
					<?php echo twentyseventeen_get_svg(array('icon'=>'instagram')); ?>
					</a>
				</li>
			<?php endif; ?>
			<?php if(get_field('artist_twitter')): ?>
				<li><a href="<?php the_field('artist_twitter'); ?>" target="_blank" title="Twitter">
					<?php echo twentyseventeen_get_svg(array('icon'=>'twitter')); ?>
					</a>
				</li>
			<?php endif; ?>
			</ul>
		</div>
			<?php
		endif;
	endif;



			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( is_single() ) : ?>
		<?php twentyseventeen_entry_footer(); ?>
	<?php endif; ?>

</article><!-- #post-## -->
